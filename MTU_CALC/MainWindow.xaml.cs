﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MTU_CALC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NetworkInterface selectedInterface = null;

        public MainWindow()
        {
            InitializeComponent();


            testLoop();
            //refreshInterfaces();                //Refreshes list of interfaces
        }

        public void refreshInterfaces()
        {
            foreach(NetworkInterface nwinterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                cbInterfaces.Items.Add(nwinterface.Name);
            }

            cbInterfaces.SelectedIndex = 0;
        }

        private void testLoop()
        {
            bool adding = true;
            bool subtracting = true;

            int target = 1500;
            int current = 68;
            int max = 1500;
            int headers = 28;

            int jumps = 100;

            var t = new System.Threading.Thread(() =>
            {
                while (adding)
                {
                    if(current != target)
                        current += jumps;

                    if (current > target)                           //If current packetsize gives an error
                    {
                        jumps = -(jumps / 10);

                        subtracting = true;
                        while (subtracting)
                        {
                            this.Dispatcher.Invoke(() =>
                            {
                                lblLocalIpv4.Content = current;
                            });

                            current += jumps;

                            if (current <= target)                  //If current packetsize no longer gives an error
                            {
                                jumps = -(jumps / 10);
                                subtracting = false;
                            }

                            System.Threading.Thread.Sleep(500);
                        }
                    }
                    else if (current == target)
                    {
                        if (current < max - headers)
                            current = current + headers;
                        else
                            current = max;

                        adding = false;
                    }

                    this.Dispatcher.Invoke(() =>
                    {
                        lblLocalIpv4.Content = current;
                    });

                    System.Threading.Thread.Sleep(500);
                }
            });

            t.Start();
        }

        private void changeInterface(object sender, SelectionChangedEventArgs e)
        {
            selectedInterface = NetworkInterface.GetAllNetworkInterfaces()[cbInterfaces.SelectedIndex];
            foreach(UnicastIPAddressInformation uipi in selectedInterface.GetIPProperties().UnicastAddresses)
            {
                if(uipi.Address.AddressFamily == AddressFamily.InterNetwork)
                {
                    lblLocalIpv4.Content = uipi.Address.ToString();
                }
                else if(uipi.Address.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    lblLocalIpv6.Content = uipi.Address.ToString();
                }
            }
        }

        private void calculateMTU(object sender, RoutedEventArgs e)
        {
            bool calculating = true;

            int loops = 1;
            int total = 0;
            int packetsize = 1200;

            lblTargetServer.Content = tbTargetServer.Text;

            var t = new System.Threading.Thread(() =>
            {
                while (calculating)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        try
                        {
                            Ping pinger = new Ping();
                            int timeout = 1000;
                            int inc = 100;
                            int dec = 100;
                            byte[] size = new byte[packetsize];
                            bool decremented = false;
                            bool incremented = false;

                            PingReply reply = pinger.Send(tbTargetServer.Text, timeout, size);

                            if (reply.Status != IPStatus.TimedOut)              //If packet size does still go through
                            {
                                if(decremented)
                                {
                                    if(dec > 1)
                                    {
                                        dec /= 10;
                                    }
                                }

                                lblTargetIp.Content = reply.Address.ToString();


                                total += (int)reply.RoundtripTime;

                                lblAvgPing.Content = (total / loops) + "ms";

                                packetsize += inc;                              //Increases Packet Size

                                lblTemp.Content = loops + "  -  " + size.Length;
                            }
                            else                                                //If packet size is too big
                            {
                                if (incremented)
                                {
                                    if(inc > 1)
                                    {
                                        inc /= 10;                              //Divides sizechange number with 10
                                    }
                                }

                                size = new byte[size.Length - dec];             //Lower Packet Size

                                lblTemp.Content = loops + "  -  " + size.Length;
                            }
                        }
                        catch (PingException pe)
                        {
                            if(pe.Message.Equals(""))
                            {
                                calculating = false;
                            }

                            lblTemp.Content = pe.Message;
                        }

                        loops++;
                        
                    });

                    System.Threading.Thread.Sleep(1000);                     //Sleep for 100ms to not spam server while calculating MTU
                }
            });

            t.Start();
        }
    }
}
